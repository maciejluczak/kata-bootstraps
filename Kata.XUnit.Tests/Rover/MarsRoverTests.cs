﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Kata.Rover;
using Kata.Rover.Data;
using Kata.Rover.Motion;
using Kata.Rover.Parser;
using NSubstitute;
using Ploeh.AutoFixture;
using Xunit;

namespace Kata.XUnit.Tests.Rover
{
    public class MarsRoverTests
    {
        private RoverState _roverState;
        public MarsRoverTests()
        {
            _roverState = new RoverState
            {
                Orientation = Orientation.North,
                Position = new Point(0,0),
                Planet = new Planet
                {
                    SizeY = 10,
                    SizeX = 10,
                    ObstacleList = new List<Point>()
                }
            };
        }

        [Fact]
        public void SequenceProcessing_WhenCalled_ValidateSequence()
        {
            List<string> commands = new List<string>() { "r", "r", "r"};
            var engineMock = Substitute.For<ICommandExecutor>();
            engineMock.ExecuteCommand("r", Arg.Any<RoverState>()).Returns((true, _roverState, null));
            engineMock.ValidateCommands(commands).Returns(true);
            var logger = Substitute.For<ILogger>();
            var sut = new MarsRover(engineMock, logger, _roverState);
            
            sut.ValidateAndProcessCommands(commands);

            engineMock.Received().ValidateCommands(commands);
        }

        [Fact]
        public void SequenceProcessing_WhenCalled_LogsCommandResult()
        {
            List<string> commands = new List<string>() { "r", "r", "r" };
            var engineMock = Substitute.For<ICommandExecutor>();
            var loggerMock = Substitute.For<ILogger>();
            var sut = new MarsRover(engineMock, loggerMock, _roverState);
            loggerMock.LogResult("r", true, _roverState, null);

            sut.ValidateAndProcessCommands(commands);

            loggerMock.Received().LogResult("r", true, _roverState, null);
        }

        [Fact]
        public void SequenceProcessing_ForValidSequence_MoveRoverInSequence()
        {
            List<string> commands = new List<string>() { "r", "f", "f", "b", "l", "f", "f" };
            var logger = Substitute.For<ILogger>();
            var commandExecutor = new CommandExecutor(new Mover(), new Rotator(), new ObstacleDetector());
            var sut = new MarsRover(commandExecutor,logger, _roverState);

            sut.ValidateAndProcessCommands(commands);

            sut.RoverState.Position.X.Should().Be(1);
            sut.RoverState.Position.Y.Should().Be(2);
            sut.RoverState.Orientation.Should().Be(Orientation.North);
        }
    }
}