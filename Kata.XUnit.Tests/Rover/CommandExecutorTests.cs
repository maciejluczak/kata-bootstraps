﻿using System.Collections.Generic;
using FluentAssertions;
using Kata.Rover.Data;
using Kata.Rover.Motion;
using Kata.Rover.Parser;
using NSubstitute;
using Xunit;

namespace Kata.XUnit.Tests.Rover
{
    public class CommandExecutorTests
    {
        private RoverState _roverState;
        public CommandExecutorTests()
        {
            _roverState = new RoverState
            {
                Orientation = Orientation.North,
                Position = new Point(0, 0),
                Planet = new Planet
                {
                    SizeY = 10,
                    SizeX = 10,
                    ObstacleList = new List<Point>()
                }
            };
        }

        [Fact]
        public void Command_Forward_CallMoveForwardMethod()
        {
            var moverMock = Substitute.For<IMover>();
            var rotateMock = Substitute.For<IRotator>();
            var obstacleDetectorMock = Substitute.For<IObstacleDetector>();
            var sut = new CommandExecutor(moverMock, rotateMock, obstacleDetectorMock);

            sut.ExecuteCommand("f", _roverState);

            moverMock.Received().MoveForwards(_roverState.Orientation, Arg.Any<Point>(), _roverState.Planet);
        }

        [Theory]
        [InlineData("b", 0)]
        [InlineData("b", 90)]
        [InlineData("b", 180)]
        [InlineData("b", 270)]
        public void Command_Backward_CallMoveBackwardMethod(string command, Orientation orientation)
        {
            var moverMock = Substitute.For<IMover>();
            var rotateMock = Substitute.For<IRotator>();
            var obstacleDetectorMock = Substitute.For<IObstacleDetector>();
            _roverState.Orientation = orientation;
            var sut = new CommandExecutor(moverMock, rotateMock, obstacleDetectorMock);

            sut.ExecuteCommand(command, _roverState);

            moverMock.Received().MoveBackwards(orientation, Arg.Any<Point>(), _roverState.Planet);
        }

        [Theory]
        [InlineData("l", RotationDirection.Left)]
        [InlineData("r", RotationDirection.Right)]
        public void Command_LeftAndRight_CallTurnMethodWithProperArguments(string command, RotationDirection rotationCommand)
        {
            var moverMock = Substitute.For<IMover>();
            var rotateMock = Substitute.For<IRotator>();
            var obstacleDetectorMock = Substitute.For<IObstacleDetector>();
            var sut = new CommandExecutor(moverMock, rotateMock, obstacleDetectorMock);

            sut.ExecuteCommand(command, _roverState);

            rotateMock.Received().Rotate(rotationCommand,0);
        }

        [Theory]
        [InlineData("f")]
        [InlineData("b")]
        public void Command_WhenCalled_ObstacleDetectionIsCalled(string command)
        {
            var moverMock = Substitute.For<IMover>();
            var rotateMock = Substitute.For<IRotator>();
            var obstacleDetectorMock = Substitute.For<IObstacleDetector>();
            var sut = new CommandExecutor(moverMock, rotateMock,obstacleDetectorMock);

            sut.ExecuteCommand(command, _roverState);

            obstacleDetectorMock.ReceivedWithAnyArgs().DetectObstacle(null,null);
        }

        [Theory]
        [InlineData(false, new[]{ "l", "q", "r"})]
        public void SequenceValidation_ForInvalidCommand_BreakOfterFoundIncorectCommand(bool expectedValue, string[] commands)
        {
            var sut = new  CommandExecutor(null,null,null);

            var result = sut.ValidateCommands(commands);

            result.Should().Be(expectedValue);
        }
    }

}
