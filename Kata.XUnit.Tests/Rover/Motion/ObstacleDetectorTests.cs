﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Kata.Rover.Data;
using Kata.Rover.Motion;
using Xunit;

namespace Kata.XUnit.Tests.Rover.Motion
{
    public class ObstacleDetectorTests
    {
        [Fact]
        public void DetectObstacle_WhenObstacleExist_ReturnTrue()
        {
            var sut  = new ObstacleDetector();

            var result = sut.DetectObstacle(new Point(0, 0), new List<Point>() { new Point(0, 0) });

            result.Should().Be(true);
        }

        [Fact]
        public void DetectObstacle_WhenObstacleDoNotExist_ReturnFalse()
        {
            var sut = new ObstacleDetector();

            var result = sut.DetectObstacle(new Point(0, 0), new List<Point>() { new Point(0, 1) });

            result.Should().Be(false);
        }
    }
}
