﻿using FluentAssertions;
using Kata.Rover.Data;
using Kata.Rover.Motion;
using Xunit;

namespace Kata.XUnit.Tests.Rover.Motion
{
    public class MoverTests
    {

        [Theory]
        [InlineData(0, 0, 0, 0, 1)]
        [InlineData(90, 0, 0, 1, 0)]
        [InlineData(180, 0, 0, 0, 9)]
        [InlineData(270, 0, 0, 9, 0)]
        [InlineData(0, 9, 0, 9, 1)]
        [InlineData(90, 9, 0, 0, 0)]
        [InlineData(180, 9, 0, 9, 9)]
        [InlineData(270, 9, 0, 8, 0)]
        public void MoveForward_PlanetSize10_MoveRoverForward(Orientation orientation, int x, int y, int expectedX, int expectedY)
        {
            var planet = new Planet {SizeY = 10, SizeX = 10};
            var starPoint = new Point(x, y);
            var sut = new Mover();

            var result = sut.MoveForwards(orientation, starPoint, planet);

            result.X.Should().Be(expectedX);
            result.Y.Should().Be(expectedY);
        }

        [Theory]
        [InlineData(0, 0, 0, 0, 9)]
        [InlineData(90, 0, 0, 9, 0)]
        [InlineData(180, 0, 0, 0, 1)]
        [InlineData(270, 0, 0, 1, 0)]
        [InlineData(180, 9, 0, 9, 1)]
        [InlineData(270, 9, 0, 0, 0)]
        [InlineData(0, 9, 0, 9, 9)]
        [InlineData(90, 9, 0, 8, 0)]
        public void MoveBackward_PlanetSize10_MoveRoverBackward(Orientation orientation, int x, int y, int expectedX, int expectedY)
        {
            var planet = new Planet {SizeY = 10, SizeX = 10};
            var starPoint = new Point(x, y);
            var sut = new Mover();

            var result = sut.MoveBackwards(orientation, starPoint, planet);

            result.X.Should().Be(expectedX);
            result.Y.Should().Be(expectedY);
        }
    }
}
