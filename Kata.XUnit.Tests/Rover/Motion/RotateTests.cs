﻿using FluentAssertions;
using Kata.Rover.Data;
using Kata.Rover.Motion;
using Xunit;

namespace Kata.XUnit.Tests.Rover.Motion
{
    public class RotateTests
    {
        [Theory]
        [InlineData(RotationDirection.Left, 0, 270)]
        [InlineData(RotationDirection.Left, 90, 0)]
        [InlineData(RotationDirection.Left, 180, 90)]
        [InlineData(RotationDirection.Left, 270, 180)]
        [InlineData(RotationDirection.Right, 0, 90)]
        [InlineData(RotationDirection.Right, 90, 180)]
        [InlineData(RotationDirection.Right, 180, 270)]
        [InlineData(RotationDirection.Right, 270, 0)]
        public void Turn_WhenCalled_ChangeOrientation(RotationDirection direction, Orientation startOrientation, int finalOrientation)
        {
            var sut  = new Rotator();

            var result = (int)sut.Rotate(direction, startOrientation);

            result.Should().Be(finalOrientation);
        }
    }
}
