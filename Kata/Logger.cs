﻿using System.IO;
using Kata.Data;

namespace Kata
{
    public interface ILogger
    {
        void LogResult(string command, bool commandWasSuccessful, RoverState marsRover, Point detectedObstacle);
        void LogCommandsValidation(string commands, bool validationWasSuccessful);
    }

    public class Logger : ILogger
    {
        private readonly TextWriter _output;

        public Logger(TextWriter output)
        {
            _output = output;
        }

        public void LogResult(string command, bool commandWasSuccessful, RoverState roverState, Point detectedObstacle)
        {
            if (commandWasSuccessful)
            {
                _output.WriteLine($"Command {command} successful");
                _output.WriteLine($"X: {roverState.Position.X}, Y: {roverState.Position.Y}");
                _output.WriteLine($"O: {roverState.Orientation}");
            }
            else
            {
                _output.WriteLine($"Obstacle was detected in point X: {detectedObstacle.X}, Y: {detectedObstacle.Y}");
            }
        }

        public void LogCommandsValidation(string commands,bool validationWasSuccessful)
        {
            if (validationWasSuccessful)
            {
                _output.WriteLine("Commands validation successful");
            }
            else
            {
                _output.WriteLine("Commands validation unsuccessful");
            }
        }
    }
}
