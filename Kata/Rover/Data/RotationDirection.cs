﻿namespace Kata.Rover.Data
{
    public enum RotationDirection
    {
        Left, Right
    }
}