﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kata.Rover.Extensions;

namespace Kata.Rover.Data
{
    public class RoverState
    {
        public Orientation Orientation { get; set; }
        public Point Position { get; set; }
        public Planet Planet { get; set; }

        public RoverState CopyRoverState()
        {
            return new RoverState
            {
                Orientation = Orientation,
                Position = new Point(Position.X, Position.Y),
                Planet = Planet
            };
        }
    }
}
