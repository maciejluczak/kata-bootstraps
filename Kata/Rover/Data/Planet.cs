﻿using System.Collections.Generic;

namespace Kata.Rover.Data
{
    public class Planet
    {
        public int SizeX { get; set; }
        public int SizeY { get; set; }
        public List<Point> ObstacleList { get; set; }
    }
}