﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kata.Rover.Data;
using Kata.Rover.Parser;
using Kata.Rover.Extensions;

namespace Kata.Rover
{
    public class MarsRover
    {
        private readonly ICommandExecutor _commandExecutor;
        private readonly ILogger _logger;
        private RoverState _roverState;

        public RoverState RoverState
        {
            get { return _roverState; }
        }

        public MarsRover(ICommandExecutor commandExecutor, ILogger logger, RoverState roverState)
        {
            _commandExecutor = commandExecutor;
            _logger = logger;
            _roverState = roverState;
        }
        
        public void ValidateAndProcessCommands(IEnumerable<string> commands)
        {
            bool validationSuccessed = _commandExecutor.ValidateCommands(commands);
            _logger.LogCommandsValidation(commands.ToString(), validationSuccessed);
            if(!validationSuccessed) return;

            ProcessCommands(commands);    
        }

        private void ProcessCommands(IEnumerable<string> commands)
        {
            foreach (var command in commands)
            {
                (var commandWasSuccessful, var modifiedRoverState, var detectedOpstacle) =
                    _commandExecutor.ExecuteCommand(command, _roverState);

                if(commandWasSuccessful) _roverState = modifiedRoverState;
                _logger.LogResult(command, commandWasSuccessful, _roverState, detectedOpstacle);
                if (!commandWasSuccessful) return;
            }
        }

    }
}