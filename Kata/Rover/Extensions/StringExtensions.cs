﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kata.Rover.Data;

namespace Kata.Rover.Extensions
{
    public static class StringExtensions
    {
        public static Orientation ToOrientation(this string orientation)
        {
            if (orientation == "N") return Orientation.North;
            if (orientation == "E") return Orientation.East;
            if (orientation == "S") return Orientation.South;
            if (orientation == "W") return Orientation.West;
            throw new ArgumentOutOfRangeException();
        }
    }
}
