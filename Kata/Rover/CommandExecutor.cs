﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kata.Rover.Data;
using Kata.Rover.Motion;

namespace Kata.Rover.Parser
{
    public interface ICommandExecutor
    {
        (bool success, RoverState modifiedRoverState, Point detectedOpstacle) ExecuteCommand(string command, RoverState data);
        bool ValidateCommands(IEnumerable<string> commands);

    }

    public class CommandExecutor : ICommandExecutor
    {
        private readonly IMover _mover;
        private readonly IRotator _rotator;
        private readonly IObstacleDetector _obstacleDetector;
        private readonly Dictionary<string, Func<RoverState, (bool, RoverState, Point)>> _commandDictionary;

        public CommandExecutor(IMover mover, IRotator rotator, IObstacleDetector obstacleDetector)
        {
            _mover = mover;
            _rotator = rotator;
            _obstacleDetector = obstacleDetector;
            _commandDictionary = new Dictionary<string, Func<RoverState, (bool, RoverState, Point)>>()
            {
                {"l", CommandLeft},
                {"r", CommandRight},
                {"f", CommandForward},
                {"b", CommandBackward}
            };
        }
        
        public (bool success, RoverState modifiedRoverState, Point detectedOpstacle) ExecuteCommand(string command, RoverState roverState)
        {
            return _commandDictionary[command](roverState.CopyRoverState());
        }

        public bool ValidateCommands(IEnumerable<string> commands)
        {
            return commands.All(command => _commandDictionary.ContainsKey(command));
        }

        private (bool success, RoverState modifiedRoverState, Point detectedOpstacle) CommandLeft(RoverState roverState)
        {
            roverState.Orientation = _rotator.Rotate(RotationDirection.Left, roverState.Orientation);
            return (true, roverState, null);
        }

        private (bool success, RoverState modifiedRoverState, Point detectedOpstacle) CommandRight(RoverState roverState)
        {
            roverState.Orientation = _rotator.Rotate(RotationDirection.Right, roverState.Orientation);
            return (true, roverState, null);
        }

        private (bool success, RoverState modifiedRoverState, Point detectedOpstacle) CommandForward(RoverState roverState)
        {
            var positionToMove = _mover.MoveForwards(roverState.Orientation, roverState.Position, roverState.Planet);
            roverState.Position = positionToMove;
            var isObstacle = _obstacleDetector.DetectObstacle(positionToMove, roverState.Planet.ObstacleList);
            return (!isObstacle, roverState, (!isObstacle)? positionToMove : null);
        }

        private (bool success, RoverState modifiedRoverState, Point detectedOpstacle) CommandBackward(RoverState roverState)
        {
            var positionToMove = _mover.MoveBackwards(roverState.Orientation, roverState.Position, roverState.Planet);
            roverState.Position = positionToMove;
            var isObstacle = _obstacleDetector.DetectObstacle(positionToMove, roverState.Planet.ObstacleList);
            return (!isObstacle, roverState, (!isObstacle) ? positionToMove : null);
        }
    }
}