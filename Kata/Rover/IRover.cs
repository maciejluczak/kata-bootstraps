﻿using System.Collections.Generic;
using Kata.Rover.Data;

namespace Kata.Rover
{
    public interface IRover
    {
        void Initialise(int x, int y, string orientation, Planet planet, List<Point> obstacleList);
        void SequenceProcess(List<string> sequenceList);
    }
}