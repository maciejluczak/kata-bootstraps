﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Kata.Rover.Data;

namespace Kata.Rover.Motion
{
    public interface IObstacleDetector
    {
        bool DetectObstacle(Point pointToCheck, List<Point> obstacleList);
    }

    public class ObstacleDetector : IObstacleDetector
    {
        public bool DetectObstacle(Point pointToCheck, List<Point> obstacleList)
        {
            var obstacle = obstacleList.FirstOrDefault(p => p.X == pointToCheck.X && p.Y == pointToCheck.Y);
            return obstacle != null;
        }
    }
}