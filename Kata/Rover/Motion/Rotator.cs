﻿using Kata.Rover.Data;

namespace Kata.Rover.Motion
{
    public interface IRotator
    {
        Orientation Rotate(RotationDirection rotationDirection, Orientation orientation);
    }

    public class Rotator : IRotator
    {
        private const int RotationAngle = 90;
        public Orientation Rotate(RotationDirection rotationDirection, Orientation orientation)
        {
            if (rotationDirection == RotationDirection.Left)
            {
                if (orientation == Orientation.North) orientation = Orientation.West;
                else orientation -= RotationAngle;
            }

            if (rotationDirection == RotationDirection.Right)
            {
                if (orientation == Orientation.West) orientation = Orientation.North;
                else orientation += RotationAngle;
            }

            return orientation;
        }
    }
}