﻿using System;
using System.Collections.Generic;
using Kata.Rover.Data;

namespace Kata.Rover.Motion
{
    public interface IMover
    {
        Point MoveForwards(Orientation orientation, Point position, Planet planet);
        Point MoveBackwards(Orientation orientation, Point position, Planet planet);
    }

    public class Mover : IMover
    {
        private readonly Dictionary<Orientation, Func<Point,Planet,Point>> _move = 
            new Dictionary<Orientation, Func<Point,Planet, Point>>()
            {
                {Orientation.North, MoveNorth},
                {Orientation.East, MoveEast},
                {Orientation.South, MoveSouth},
                {Orientation.West, MoveWest}
            };

        public Point MoveBackwards(Orientation orientation, Point startPosition, Planet planet)
        {
            orientation =(Orientation) (((int)orientation + 180) % 360);
            return _move[orientation](startPosition, planet);
        }

        public Point MoveForwards(Orientation orientation, Point startPosition, Planet planet)
        {
            return _move[orientation](startPosition, planet);    
        }

        private static Point MoveEast(Point startPoint, Planet planet)
        {
            return startPoint.X == planet.SizeX - 1 ?
                new Point(0, startPoint.Y) :
                new Point(startPoint.X + 1, startPoint.Y);
        }
        private static Point MoveNorth(Point startPoint, Planet planet)
        {
            return startPoint.Y == planet.SizeY - 1 ?
                new Point(startPoint.X, 0) :
                new Point(startPoint.X, startPoint.Y + 1);
        }
        private static Point MoveWest(Point startPoint, Planet planet)
        {
            return startPoint.X == 0
                ? new Point(planet.SizeX - 1, startPoint.Y)
                : new Point(startPoint.X - 1, startPoint.Y);
        }
        private static Point MoveSouth(Point startPoint, Planet planet)
        {
            return startPoint.Y == 0 ?
                new Point(startPoint.X, planet.SizeY - 1) :
                new Point(startPoint.X, startPoint.Y - 1);
        }
    }
}