﻿namespace Kata.Rover
{
    public interface IRover
    {
        void moveForward();
        Point getPosition();
    }
}