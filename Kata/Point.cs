﻿namespace Kata.Rover
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        public bool Obstacle { get; set; }
        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}