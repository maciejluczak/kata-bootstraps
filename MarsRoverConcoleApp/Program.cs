using System;
using System.Collections.Generic;
using Kata;
using Kata.Rover;
using Kata.Rover.Data;
using Kata.Rover.Extensions;
using Kata.Rover.Motion;
using Kata.Rover.Parser;

namespace MarsRoverConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string orientation = "N";
            List<string> commands = new List<string>(){ "l", "f", "f", "b", "f", "l" ,"b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b","b", "b" };
            ICommandExecutor engine = new CommandExecutor(new Mover(), new Rotator(), new ObstacleDetector());
            var logger = new Logger(Console.Out);
            var roverState = new RoverState
            {
                Planet = new Planet()
                {
                    SizeY = 10,
                    SizeX = 10,
                    ObstacleList = new List<Point>() { new Point(8, 4) }
                },
                Orientation = orientation.ToOrientation(),
                Position = new Point(0, 0)
            };
            var rover = new MarsRover(engine, logger, roverState);
            try
            {
                rover.ValidateAndProcessCommands(commands);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }
    }
}
